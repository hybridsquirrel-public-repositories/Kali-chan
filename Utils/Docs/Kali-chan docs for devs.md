# Kali-chan
A discord bot for SIH 2021 server

# File stucture
>├── Commands  
>│   ├── compgen.js  
>│   ├── help.js  
>│   ├── man.js  
>│   ├── ping.js  
>│   ├── poll.js  
>│   ├── *schdule.js  
>│   ├── schedule.js  
>│   └── *template.js  
>├── config.json  
>├── Databases  
>│   └── Schedular  
>├── Dockerfile  
>├── Docs  
>│   ├── Commads  
>│   ├── Databases  
>│   ├── Kali-chan docs for devs.md  
>│   └── Source  
>├── main.js  
>├── package.json  
>├── package-lock.json  
>├── README.md  
>└── start.sh