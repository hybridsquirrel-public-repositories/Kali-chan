#!/bin/bash
#--------------------------------------------------
#
# If a collaborator changes this file,
# the collaborator will be purged from the project.
#
#--------------------------------------------------







#MERGECOUNTCOMPARE=$( curl --header 'Content-Type: application/json' --request POST --data '{"query": "query {project(fullPath: \"hybridsquirrel-public-repositories/DiscordBot\"){mergeRequests{count}}}"}' https://gitlab.com/api/graphql --silent | jq '.data.project.mergeRequests.count' )
#
#echo "Starting Script..."
#
#while true
#do
#  MERGECOUNTUPDATE=$( curl --header 'Content-Type: application/json' --request POST --data '{"query": "query {project(fullPath: \"hybridsquirrel-public-repositories/DiscordBot\"){mergeRequests{count}}}"}' https://gitlab.com/api/graphql --silent | jq '.data.project.mergeRequests.count' )
#  echo "is looping"
#  if [ "$MERGECOUNTCOMPARE" -lt "$MERGECOUNTUPDATE" ]
#  then
#    MERGECOUNTCOMPARE=$( curl --header 'Content-Type: application/json' --request POST --data '{"query": "query {project(fullPath: \"hybridsquirrel-public-repositories/DiscordBot\"){mergeRequests{count}}}"}' https://gitlab.com/api/graphql --silent | jq '.data.project.mergeRequests.count' )
#    git switch deployment
#    git pull

#    docker stop $(docker ps -q)
#    docker system prune -a -f
#    docker build -t discordbotimage .
#    docker run -d --name discordbotcontainer discordbotimage

#  fi

#  if [ ! "$(docker ps -q -f name=discordbotcontainer)" ]; then
#      if [ "$(docker ps -aq -f status=exited -f name=discordbotcontainer)" ]; then
          # cleanup
#          docker rm discordbotcontainer
          # run your container
#          docker run -d --name discordbotcontainer my-docker-image

#      fi
#  fi

#  sleep 25
#done


# sudo systemctl daemon-reload
# sudo systemctl restart docker
# sudo nano /etc/resolv.conf