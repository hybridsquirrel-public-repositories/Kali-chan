'use strict';
//Setup Discord Bot
const {Client, Intents, Collection} = require('discord.js');
const intents = new Intents(32767);
const client = new Client({ intents });
const config = require('./config.json');


//Setup importing commands from external files
const fs = require('fs');
client.commands = new Collection();
const commandFiles = fs.readdirSync('./Commands/').filter(file => file.endsWith('.js'));
for(const file of commandFiles){
    const command = require(`./Commands/${file}`);
    client.commands.set(command.name, command);
}


//Logs bot being activated
client.on('ready', () => {
    console.log(`${client.user.tag} is Online`);
    client.user.setActivity('Birds',{type: 'WATCHING'})

});


//Logging channel activity to console
client.on('messageCreate', (message) => {
    console.log( `${message.author.id}| |${message.author.username}: ${message.content} -> Channel: ${message.channel.name}`);
});


//Executes Commands
client.on('messageCreate', (message) => {
    if(message.content.startsWith(config.Prefix)){
        //Configuring Inputs for Commands and Flags
        let msg = message.content.toLowerCase();
        let args = msg.substring(config.Prefix.length).split(" ");
        try{ client.commands.get(`${args[0]}`).execute(client, message) }
        catch (e) {

            console.log(e)


            //console.log("Can't find command file")
            //message.channel.send("Can't find command")
        }



    }
});


//Login in to discord bot
client.login(config.Token).then(() =>{
    console.log('Logged in!')
});


//Urls Delete later
//https://discordjs.guide/creating-your-bot/
//https://discordjs.guide/
//https://discord.js.org/#/docs/main/stable/general/welcome
