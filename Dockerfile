#--------------------------------------------------
#
# If a collaborator changes this file,
# the collaborator will be purged from the project.
#
#--------------------------------------------------

FROM node:alpine

WORKDIR /sihbot

COPY package.json .

RUN npm i

COPY . .

CMD ["node", "main.js"]