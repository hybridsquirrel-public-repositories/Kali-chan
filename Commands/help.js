const { MessageEmbed } = require('discord.js')
const os = require('os')
const config = require("./../config.json")


module.exports = {
    name: 'help',
    desciption: 'helps user get started with the bot and gives system status. \n' +
        'Usage: !help',
    execute(client, message){
        const helpString = `
        ${config.Name}, Version: ${config.Version} 
        Running on: ${os.hostname()} ${os.type()} ${os.arch()}
        ${os.hostname()} uptime: ${Math.round(((os.uptime() / 60) / 60))}h
        
        Type "!man [command]" to reference manual
        Type "!compgen" to list commands
        A star (*) next to a name means that the command is disabled.
        `

        const helpEmbed = new MessageEmbed()
                .setColor('#009B77')
                .setTitle("Help")
                .setDescription(
                    `${helpString}`
                )
                .setTimestamp()

        message.channel.send({embeds: [helpEmbed]})

    }
}
