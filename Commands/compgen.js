const fs = require("fs");
module.exports = {
    name: 'compgen',
    desciption: 'compgen lists all available and unavailable commands. \n' +
        'A star (*) next to a name means that the command is disabled. \n' +
        'Usage: !compgen',
    execute(client, message){

        //Looping command files
        let commandList = ""
        const commandFiles = fs.readdirSync('./Commands/').filter(file => file.endsWith('.js'));
        for(const file of commandFiles){
            if(!file.startsWith('*')){
                commandList += `${file.slice(0,-3)} \n`
            }

        }
        for(const file of commandFiles){
            if(file.startsWith('*')){
                commandList += `${file.slice(0,-3)} \n`
            }

        }
        message.channel.send(commandList)

    }
}
