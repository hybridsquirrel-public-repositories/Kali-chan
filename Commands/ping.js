module.exports = {
    name: 'ping',
    desciption: 'pings the Bot and API for Latency \n' +
        'Usage: !ping',
    execute(client, message){
        message.channel.send(`Pong! Latency is ${Date.now() - message.createdTimestamp} ms, ` +
        `API Latency is ${Math.round(client.ws.ping)} ms`);
    }
}
