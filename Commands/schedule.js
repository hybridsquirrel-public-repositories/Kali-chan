const schedular = require('./../Databases/Schedular/Schedular.json')
const Canvas = require('canvas')
const config = require("./../config.json")
const {MessageEmbed, MessageAttachment} = require("discord.js")
const {registerFont} = require("canvas");

module.exports = {
    name: 'schedule',
    desciption: 'shows class schedule' + '\n!schedule \n!schedule [week number]',
    async execute(client, message){
        registerFont('../Kali-chan/Utils/OpenSans-VariableFont_wdth,wght.ttf', { family: 'OpenSans' })

        const args = message.content.substring(config.Prefix.length).split(" ");
        if(args.length === 1){
            try {
                await message.channel.send("Week: " + getWeek()[1])
                await drawWeek(message, getWeek())
            }
            catch (e) {
                message.channel.send("Has currently not been Scheduled or Internal error")
            }

        }
        else {
            try {
                await message.channel.send("Week: " + args[1])
                await drawWeek(message, args)
            }
            catch (e){
                message.channel.send("User input error or Internal error")

            }

        }




    }
}

// Dont touch its a miracle that this works
async function drawWeek(message, args){
    const Weekday = ["Mon", "Tue", "Wed", "Thu", "Fri"]

    // Creates base image
    const canvas = Canvas.createCanvas(700, 400)
    const ctx = canvas.getContext('2d')
    ctx.fillStyle = "#424549"
    ctx.fillRect(0,0, canvas.width, canvas.height)


    // It works (don't fix it if it's not broken)
    for(let i = 0; i < 5; i++){
        if(i === 0) {
            const txt = ctx.measureText(Weekday[i]).width
            ctx.fillStyle = "#009B77"
            ctx.fillRect(0, 0, 700, 70)
            ctx.font = "40px 'OpenSans'"
            ctx.fillStyle = '#fff'
            ctx.fillText(Weekday[i], 30, 50)
            for(let j = 0; j < 4; j++) {
                console.log(schedular[args[1]][i][j])
                if(typeof schedular[args[1]][i][j] !== "undefined"){
                    ctx.font = "20px 'OpenSans'"
                    ctx.fillStyle = '#fff'
                    ctx.fillText(schedular[args[1]][i][j].replaceAll(" ", "\n"), 10, 100 * (j+1))
                }
            }

        }
        else{
            ctx.beginPath();
            ctx.moveTo(140 * i, 0)
            ctx.lineTo(140 * i , 400)
            ctx.lineWidth = 4;
            ctx.stroke()

            ctx.font = "40px 'OpenSans'"
            ctx.fillStyle = '#fff'
            ctx.fillText(Weekday[i], (140 * i)+30, 50)
            for(let j = 0; j < 4; j++){
                console.log(schedular[args[1]][i][j])
                if(typeof schedular[args[1]][i][j] !== "undefined"){
                    ctx.font = "20px 'OpenSans'"
                    ctx.fillStyle = '#fff'
                    ctx.fillText(schedular[args[1]][i][j].replaceAll(" ", "\n"), 10 + (140*i), 100 * (j+1))
                }
            }
        }
    }


    // Add image to buffer then sends it
    const messageAttachment = new MessageAttachment(canvas.toBuffer(), "Schedule.png")
    await message.channel.send({files: [messageAttachment]})

}

function getWeek (d = new Date()) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
    // Get first day of year
    let yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    const weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return [d.getUTCFullYear(), weekNo];
}