const {MessageEmbed} = require("discord.js");
const fs = require("fs");
const config = require("./../config.json");

module.exports = {
    name: 'man',
    desciption: 'a manual for commands. \n' +
        'Usage: !man [command]',
    execute(client, message){
        const args = message.content.substring(config.Prefix.length).split(" ");
        const manEmbed = new MessageEmbed()
            .setColor('#009B77')
            .setTitle("Help")
            .setDescription(
                `${client.commands.get(`${args[1]}`).desciption}`
            )
            .setTimestamp()
        message.channel.send({embeds: [manEmbed]})
    }
}
