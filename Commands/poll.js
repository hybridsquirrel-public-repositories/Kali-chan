const config = require("./../config.json")
const { promisify } = require('util')
const sleep = promisify(setTimeout)

const { MessageEmbed } = require('discord.js')
const emojiList = ["0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟"]
//Poll channel
channelID = ""

module.exports = {
    name: 'poll',
    desciption: 'creates a poll. \n' +
        'Usage: !poll [question (no spaces allowed)] [option (1 - 11)]... ',
    execute(client, message){

        if(message.content === "!poll" || message.content === "!poll "){
            message.channel.send("Need arguments")
        }
        else{
            sendVoteEmbed(message).then()
        }

        async function sendVoteEmbed(){
            const args = message.content.substring(config.Prefix.length).split(" ");
            let pollMessage = ""
            for(let i = 0; i < args.length - 2; i++) {
                pollMessage += emojiList[i]
                pollMessage += " :>: "
                if(i + 2 < args.length){
                    pollMessage += args[i + 2]
                    pollMessage += "\n\n"
                }
            }

            const voteEmbed = new MessageEmbed()
                .setColor('#009B77')
                .setTitle(args[1])
                .setDescription(
                    `${pollMessage}`
                )
                .setTimestamp()

            let msgEmbed = await message.channel.send({embeds: [voteEmbed]})
            for(let j = 0; j < args.length - 2; j++){
                await msgEmbed.react(emojiList[j])
                await sleep(500)
            }
            message.delete()
            // Add to database
        }



    }
}
//https://codelog.network/2020/08/30/discord-js-get-users-who-reacted-to-old-messages/
//https://discordjs.guide/popular-topics/reactions.html
//https://discordjs.guide/additional-info/changes-in-v13.html
//https://www.youtube.com/watch?v=pP6omkdLfGEs